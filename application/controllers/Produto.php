<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->library('Crud');
        $this->load->model('DBModel');
        $this->load->library('component/Card', 'card');
    }

    public function index(){
        $this->load->view('common/header');
        $this->load->view('others/mdb');
        $this->load->view('common/footer');
    }

    public function catalogo(){
				$this->load->view('common/header');
				$this->load->view('home/navbar_home');
				$this->load->view('others/jumbotron');
				$data['cards'] = $this->DBModel->Jumbo('produto');
				$this->load->view('others/teste', $data);
				$this->load->view('common/footer');
		}

    public function formCadastro(){
         $this->DBModel->salvar('produto');
         $html = $this->load->view('produto/formProduto', null, true);

         $this->show($html);
     }

     public function edit($id){
         $this->DBModel->atualizar('produto', $id);
         $this->DBModel->carregar('produto', $id);

         $html = $this->load->view('produto/formProduto', null, true);
         $this->show($html);
     }

     public  function delete($id){
         $this->DBModel->delete('produto', $id);
         redirect('administration/');
     }
}
