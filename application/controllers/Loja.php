<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Loja extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('LojaModel', 'loja');
    }

    public function index(){
        $html = $this->loja->lista_produtos();
        $this->show($html);
    }

    public function cadastro(){
        $this->loja->salvar();

        $html = $this->load->view('loja/cadastro', null, true);
        $this->show($html);
    }

    public function edit($id){
        $this->loja->atualizar($id);
        $this->loja->carregar($id);

        $html = $this->load->view('loja/cadastro', null, true);
        $this->show($html);
    }

    public  function delete($id){
        $this->loja->delete($id);
        redirect('loja');
    }

}
