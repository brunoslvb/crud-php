<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->library('Crud');
        $this->load->model('DBModel');
    }

    public function index(){
      $html = $this->load->view('cliente/formCadastro', null, true);
      $this->show($html);
    }

   public function formCadastro(){
        $this->DBModel->salvar('cliente');
        $html = $this->load->view('cliente/formCadastro', null, true);

        $this->show($html);
    }

    public function edit($id){
        $this->DBModel->atualizar('cliente', $id);
        $this->DBModel->carregar('cliente', $id);

        $html = $this->load->view('cliente/formCadastro', null, true);
        $this->show($html);
    }

    public  function delete($id){
        $this->DBModel->delete('cliente', $id);
        redirect('loja');
    }
}
