<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validator extends CI_Object{

  public function getValidate($table){
      if ($table == 'cliente') {
          $this->validator->cliente_validate();
      } elseif ($table == 'home') {
          $this->validator->home_validate();
      }
  }

    public function cliente_validate(){
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required|min_length[4]|max_length[60]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[10]|max_length[150]');
        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|required');
        return $this->form_validation->run();
    }

    public function home_validate(){
        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required|min_length[5]|max_length[60]');
        $this->form_validation->set_rules('subtitulo', 'Subtitulo', 'trim|required|min_length[10]|max_length[100]');
        return $this->form_validation->run();
    }

    public function produto_validate(){
        $this->form_validation->set_rules('nome', 'Nome do produto', 'trim|required|min_length[4]|max_length[40]');
        $this->form_validation->set_rules('descricao', 'descr do prod', 'trim|required|min_length[10]|max_length[512]');
        $this->form_validation->set_rules('preco', 'preço do produto', 'trim|required|decimal|greater_than[0]');
        return $this->form_validation->run();
    }

}
