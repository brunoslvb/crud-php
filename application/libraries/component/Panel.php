<?php

class Panel {
    public function tablePanel($tabelas){
        $html = '
            <ul class="nav nav-pills nav-justified border" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Produtos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Clientes</a>
                </li>
            </ul>

            <div class="col-md-12 mx-auto">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        '.$tabelas['produtos'].'
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        '.$tabelas['clientes'].'
                    </div>
                </div>
            </div>
        ';

        return $html;
    }
}
