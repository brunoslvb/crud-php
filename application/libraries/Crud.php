<?php

class Crud extends CI_Object {

    public function get($table){
        $rs = $this->db->get($table);
        return $rs->result_array();
    }

    public function get_by_id($table, $id){
        $cond['id'] = $id;
        $rs = $this->db->get_where($table, $cond);
        return $rs->row_array();
    }

    public function get_where($table, $column, $value){
        $cond[$column] = $value;
        $rs = $this->db->get_where($table, $cond);
        return $rs->result_array();
    }

    public function set($table, Array $data){
        $this->db->insert($table, $data);
    }

    public function update($table, $data, $id){
        $cond['id'] = $id;
        return $this->db->update($table, $data, $cond);
    }

    public function delete($table, $id){
        $cond = array('id' => $id);
        $this->db->delete($table, $cond);
    }
}
