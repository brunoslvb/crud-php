<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class LojaModel extends CI_Model{

    function __construct(){
        $this->load->library('util/Validator');
    }

    public function lista_produtos(){
        $this->load->library('Produto');
        $v = $this->produto->get();
        
        $html = '<br><table class="table mt-5">';
        $html .= $this->table_header();
        $html .= $this->table_body($v);
        $html .= '</table>';
        return $html;
    }

    private function table_body($v){
        $html = '';
        foreach ($v as $produto) {
            $html .= '<tr>';
            $html .= '<td>'.$produto['id'].'</td>';
            $html .= '<td>'.$produto['nome'].'</td>';
            $html .= '<td>'.$produto['descricao'].'</td>';
            $html .= '<td>'.$produto['preco'].'</td>';
            $html .= '<td width="120px">'.$this->action_buttons($produto['id']).'</td>';
            $html .= '</tr>';
        }
        return $html;
    }

    private function action_buttons($id){
        $html = '<a href="'.base_url('loja/edit/'.$id).'">';
        $html .= '<i class="far fa-edit indigo-text mr-3"></i></a>';
        $html .= '<a href="'.base_url('loja/delete/'.$id).'">';
        $html .= '<i class="far fa-trash-alt red-text"></i></a>';
        return $html;
    }

    private function table_header(){
        $html  = '<tr>';
        $html .= '<td>#</td>';
        $html .= '<td>Nome</td>';
        $html .= '<td>Descrição</td>';
        $html .= '<td>Preço</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        return $html;
    }

    public function salvar(){
        if(sizeof($_POST) == 0) return;
        // depois de verificar que o post tem dados
        // verifique a correção dos dados recebidos
        // crie as regras de validação
        
        if($this->validator->produto_validate()){
            $data['nome'] = $this->input->post('nome');
            $data['descricao'] = $this->input->post('descricao');
            $data['preco'] = $this->input->post('preco');
    
            $this->load->library('Produto');
            $this->produto->set($data);
        }
    }

    public function carregar($id){
        $this->load->library('Produto');
        $_POST = $this->produto->get_by_id($id);
    }

    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        if($this->validator->produto_validate()){
            $data['nome'] = $this->input->post('nome');
            $data['descricao'] = $this->input->post('descricao');
            $data['preco'] = $this->input->post('preco');

            $this->load->library('Produto');
            $status = $this->produto->update($data, $id);
            if($status) redirect('loja');
        }
    }

    public function delete($id){
        $this->load->library('Produto');
        $this->produto->delete($id);
    }
}