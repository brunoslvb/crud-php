<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClienteModel extends CI_Model{

    function __construct(){
        $this->load->library('util/Validator');
    }

    public function getColumns(){
        $columns = array('id', 'nome', 'email', 'telefone');
        return $columns;
    }
}
