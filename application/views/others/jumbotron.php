<!-- Jumbotron -->
<div class="card card-image" style="background-image: url('http://www.arqbox.com.br/wp-content/uploads/2017/09/banner-ROUPAS.png'); background-size: 100%;">
  <div class="text-white text-center rgba-stylish-strong py-5 px-4">
    <div class="py-5">

      <!-- Content -->
      <h2 class="card-title h2 my-4 py-2">Fique admirado com nosso catálogo de produtos</h2>
      <p class="mb-4 pb-2 px-md-5 mx-md-5">Produtos confeccionados com os melhores materias para agradar e conquistar nossos clientes</p>

    </div>
  </div>
</div>
<!-- Jumbotron -->
