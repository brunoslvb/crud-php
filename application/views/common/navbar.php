
<!--Navbar -->
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="<?= site_url('/') ?>">Fashion Modas</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('produto/catalogo') ?>"><i class="fas fa-list"></i> Catálogo</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('contato/') ?>"><i class="far fa-building"></i> Contato</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="<?= site_url('administration/') ?>">
          <i class="fas fa-cog fa-spin"></i>
        </a>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->
