
<div class="container col-md-5 mt-4">
<?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
<form class="text-center border border-light p-5" method="POST">
    <p class="h4 mb-4">Cadastre-se</p>
    <div class="form-row mb-4">
        <div class="col">
            <input type="text" id="titulo" name="titulo" class="form-control" placeholder="Título" value="<?= set_value('titulo') ?>">
        </div>
        <div class="col">
            <input type="text" id="subtitulo" name="subtitulo" class="form-control" placeholder="Subtítulo" value="<?= set_value('subtitulo') ?>">
        </div>
    </div>
    <button class="btn btn-info my-4 btn-block" type="submit">Cadastrar</button>
</form>
</div>
