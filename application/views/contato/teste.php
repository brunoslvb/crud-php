<div class="view jarallax background-home" style="background-image: url('https://cdn.pixabay.com/photo/2016/11/18/13/48/clothes-1834650_960_720.jpg');background-repeat: no-repeat; background-size: 100%; padding-top: 100px;">

  <div class="mask rgba-black-strong">

    <div class="container col-md-12 p-5 white-text">

        <h1 class="text-center mt-5">Naah Fashion Modas</h1>

        <p class="text-center mx-auto mt-4" style="font-size: 18px;">Empresa criada em 2019 com o objetivo de levar ao público roupas com qualidade por um preço agrada qualquer um. </p>

        <hr>

        <div class="row mt-5 text-center">
          <div class="col-sm-4 mb-3 mb-md-0">
            <div class="card">
              <div class="card-header black white-text">Missão</div>
              <div class="card-body">
                <h5 class="card-title black-text"></h5>
                <p class="card-text">Levar produtos de qualidade para pessoas de todas as classes.</p>
              </div>
            </div>
          </div>

          <div class="col-sm-4 mt-5">
            <div class="card">
              <div class="card-header black white-text">Visão</div>
              <div class="card-body">
                <h5 class="card-title black-text"></h5>
                <p class="card-text">Ser reconhecida nacionalmente visando sempre agradar os clientes</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header black white-text">Valor</div>
              <div class="card-body">
                <p class="card-text">Buscamos conquistar nossos clientes sendo íntegros, comprometidos e responsáveis com nossas promessas.</p>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
</div>
